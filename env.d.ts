/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_TITLE: string; //系统名称
  readonly VITE_BASE_API: string;
  readonly VITE_USE_MOCK: 'true' | 'false'; //mock开关
  readonly VITE_THEME: 'light' | 'dark'; //导航栏主题
  // 更多环境变量...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}

declare const __APP_INFO__: {
  pkg: {
    name: string; //项目名称
    version: string; //当前版本
    dependencies: Recordable<string>; //项目依赖
    devDependencies: Recordable<string>; //开发依赖
  };
  lastBuildTime: string; //最近编译时间
};
