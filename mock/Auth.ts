import { MockMethod } from 'vite-plugin-mock';

export default [
  {
    url: '/api/captcha',
    method: 'get',
    timeout: 100,
    rawResponse: (req, res) => {
      res.end('ok');
    },
  },
  {
    url: '/api/login',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
        data: [
          {
            tokenName: 'Authorization',
            tokenValue: 'syssMzeEjnSfCW4a1Xu5VlREPFQHIl2NcSL9leCIRn6L1CkIB4P2LCRoRpN6rwVL',
          },
        ],
        total: 1,
      };
    },
  },
  {
    url: '/api/logout',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/current',
    method: 'get',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
        data: [
          {
            userId: 10000,
            nickname: '超级管理员',
            locked: false,
            permissions: ['*'],
          },
        ],
        total: 1,
      };
    },
  },
] as MockMethod[];
