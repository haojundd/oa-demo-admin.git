import { MockMethod } from 'vite-plugin-mock';
import Mock from 'mockjs';

const list = [];
const count = 20;

for (let i = 0; i < count; i++) {
  list.push(
    Mock.mock({
      roleId: '@increment',
      roleName: '@cword(5)',
      roleSort: '@increment',
      createTime: '@datetime',
      updateTime: '@datetime',
    })
  );
}

export default [
  {
    url: /\/api\/role\/page\/\d+\/\d+/,
    method: 'post',
    timeout: 300,
    response: () => {
      const pageIndex = 1;
      const pageSize = 10;
      const pageList = list.filter(
        (item, index) => index < pageSize * pageIndex && index >= pageSize * (pageIndex - 1)
      );

      return {
        code: 'SUCCESS',
        message: '成功',
        data: pageList,
        total: count,
      };
    },
  },
  {
    url: '/api/role/list',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
        data: list,
        total: count,
      };
    },
  },
  {
    url: '/api/role',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/role/delete',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/role',
    method: 'put',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: /\/api\/role\/checked\/\d+/,
    method: 'get',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
        total: 14,
      };
    },
  },
] as MockMethod[];
