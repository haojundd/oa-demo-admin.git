import { MockMethod } from 'vite-plugin-mock';
import Mock from 'mockjs';

const list = [];
const count = 20;

for (let i = 0; i < count; i++) {
  list.push(
    Mock.mock({
      operId: '@increment',
      requestMethod: '@word',
      operUrl: '@url',
      className: '@domain',
      method: '@domain',
      'status|0-1': 0,
      operName: '@cname',
      errorMsg: '@cword(5)',
      operTime: '@datetime',
    })
  );
}

export default [
  {
    url: /\/api\/operLog\/page\/\d+\/\d+/,
    method: 'post',
    timeout: 300,
    response: () => {
      const pageIndex = 1;
      const pageSize = 10;
      const pageList = list.filter(
        (item, index) => index < pageSize * pageIndex && index >= pageSize * (pageIndex - 1)
      );

      return {
        code: 'SUCCESS',
        message: '成功',
        data: pageList,
        total: count,
      };
    },
  },
  {
    url: '/api/operLog/truncate',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
  {
    url: '/api/operLog/delete',
    method: 'post',
    timeout: 300,
    response: () => {
      return {
        code: 'SUCCESS',
        message: '成功',
      };
    },
  },
] as MockMethod[];
