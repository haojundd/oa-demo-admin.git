import './styles/index.less';
import { createApp } from 'vue';
import App from './App.vue';
import { setupRouter } from './router';
import { setupStore } from '@/store';

const app = createApp(App);

// 配置 store
setupStore(app);

// 挂载路由
setupRouter(app);

app.mount('#app');
