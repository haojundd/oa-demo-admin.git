import { VNode } from 'vue';

export {};

declare module 'vue-router' {
  interface RouteMeta extends Record<string | number | symbol, unknown> {
    title?: string; //页面标题
    permissions?: string[]; //页面权限
    icon?: () => VNode; //图标
    hidden?: boolean; //是否在菜单栏隐藏
    alwaysShow?: boolean; //只有一个子菜单是否显示自身
    keepAlive?: boolean; //是否缓存页面
  }
}
