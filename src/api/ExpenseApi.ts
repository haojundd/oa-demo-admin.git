import request from './http';

/**
 * 报销表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class ExpenseApi {
  static page(data: Expense) {
    let { index, size, ...vo } = data;
    return request.post(`/expense/page/${index}/${size}`, vo);
  }

  static list(data: Expense) {
    return request.post('/expense/list', data);
  }

  static getById(id: number) {
    return request.get(`/expense/${id}`);
  }

  static save(data: Expense) {
    return request.post('/expense', data);
  }

  static update(data: Expense) {
    return request.put('/expense', data);
  }

  static delete(ids: number[]) {
    return request.post('/expense/delete', ids);
  }
}
