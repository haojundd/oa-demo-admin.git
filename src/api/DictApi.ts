import request from './http';

/**
 * 字典表 前端接口
 *
 * @author haojun
 * @since 2024-01-03
 */
export default class DictApi {
  static page(data: Dict) {
    let { index, size, ...vo } = data;
    return request.post(`/dict/page/${index}/${size}`, vo);
  }

  static list(data: Dict) {
    return request.post('/dict/list', data);
  }

  static getById(id: number) {
    return request.get(`/dict/${id}`);
  }

  static save(data: Dict) {
    return request.post('/dict', data);
  }

  static update(data: Dict) {
    return request.put('/dict', data);
  }

  static delete(ids: number[]) {
    return request.post('/dict/delete', ids);
  }
}
