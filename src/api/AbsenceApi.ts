import request from './http';

/**
 * 请假表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class AbsenceApi {
  static page(data: Absence) {
    let { index, size, ...vo } = data;
    return request.post(`/absence/page/${index}/${size}`, vo);
  }

  static list(data: Absence) {
    return request.post('/absence/list', data);
  }

  static getById(id: number) {
    return request.get(`/absence/${id}`);
  }

  static save(data: Absence) {
    return request.post('/absence', data);
  }

  static update(data: Absence) {
    return request.put('/absence', data);
  }

  static delete(ids: number[]) {
    return request.post('/absence/delete', ids);
  }
}
