import request from './http';

/**
 * 岗位表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class PostApi {
  static page(data: Post) {
    let { index, size, ...vo } = data;
    return request.post(`/post/page/${index}/${size}`, vo);
  }

  static list(data: Post) {
    return request.post('/post/list', data);
  }

  static getById(id: number) {
    return request.get(`/post/${id}`);
  }

  static save(data: Post) {
    return request.post('/post', data);
  }

  static update(data: Post) {
    return request.put('/post', data);
  }

  static delete(ids: number[]) {
    return request.post('/post/delete', ids);
  }
}
