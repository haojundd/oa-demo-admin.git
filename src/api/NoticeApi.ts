import request from './http';

/**
 * 通知表 前端接口
 *
 * @author haojun
 * @since 2023-12-13
 */
export default class NoticeApi {
  static page(data: Notice) {
    let { index, size, ...vo } = data;
    return request.post(`/notice/page/${index}/${size}`, vo);
  }

  static list(data: Notice) {
    return request.post('/notice/list', data);
  }

  static getById(id: number) {
    return request.get(`/notice/${id}`);
  }

  static save(data: Notice) {
    return request.post('/notice', data);
  }

  static update(data: Notice) {
    return request.put('/notice', data);
  }

  static delete(ids: number[]) {
    return request.post('/notice/delete', ids);
  }
}
