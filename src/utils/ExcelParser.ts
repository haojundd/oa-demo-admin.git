import { read, utils } from 'xlsx';

export default class ExcelParser {
  /**
   * 解析excel
   */
  static parse(file: File | null) {
    return new Promise((resolve, reject) => {
      if (!file) {
        reject();
        return;
      }
      let reader = new FileReader();
      reader.readAsArrayBuffer(file);
      reader.onloadend = () => {
        const arrayBuffer = reader.result;
        const workbook = read(arrayBuffer, { type: 'array' });
        const csv = utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
        resolve(csv);
      };
    });
  }
}
